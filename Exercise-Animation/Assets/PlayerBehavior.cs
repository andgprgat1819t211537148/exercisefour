﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour
{

    float velocity;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        velocity = speed * Time.deltaTime;
        velocity *= 40f;

        if (Input.GetKey(KeyCode.W))
        {
            this.GetComponent<Animator>().SetInteger("Direction", 3);
            this.GetComponent<Animator>().SetBool("IsOnTheGround", false);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, (velocity / 2.0f)));
        }
        else if(Input.GetKeyUp(KeyCode.W))
        {
            this.GetComponent<Animator>().SetInteger("Direction", 4);
            this.GetComponent<Animator>().SetBool("IsOnTheGround", false);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, (velocity / 2.0f)));
        }
       
        else if (Input.GetKey(KeyCode.S))
        {
            
            this.GetComponent<Animator>().SetBool("IsMoving", false);
            this.GetComponent<Animator>().SetInteger("Direction", 2);
            print("Crouch");
        }

        else if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Animator>().SetBool("IsMoving", true);
            this.GetComponent<Animator>().SetInteger("Direction", 1);
            print("Walk Left");
            this.GetComponent<SpriteRenderer>().flipX = true;

            
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-velocity, 0));

        }

        else if (Input.GetKey(KeyCode.D))
        {

            this.GetComponent<Animator>().SetBool("IsMoving", true);
            this.GetComponent<Animator>().SetInteger("Direction", 1);
            print("Walk Right");
            this.GetComponent<SpriteRenderer>().flipX = false;

            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(velocity, 0));
        }

        else
        {
            this.GetComponent<Animator>().SetBool("IsMoving", false);
            
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            this.GetComponent<Animator>().SetBool("IsOnTheGround", true);
        }            
    }
}
